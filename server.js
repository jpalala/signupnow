const hapi = require('hapi');

const server = hapi.server({
    port: 8081,
    host: 'localhost'
});
// route
server.route({
            config: {
                cors: {
                    origin: ['*'],
                    additionalHeaders: ['cache-control', 'x-requested-with']
                }
            },
            method: 'POST',
            path: '/api',
            handler: (req, h) => {
                const data = { message: "OK" }

                return h.response(data).code(200)
            })

        const bootUpServer = async() => {
            await server.start();
            console.log(`Server is running at ${server.info.uri}`)

            process.on('unhandledRejection', (err) => {
                console.log(err);
                process.exit(1);
            })
        }

        bootUpServer();